﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UDV_TestApplication.Entities
{
    [Table("profileInfo")]
    public class ProfileInfo
    {
        [Key]
        [Column("profileID")]
        public long Id { get; set; }

        [Column("domain")]
        public required string Domain { get; set; }

        [Column("lastPostID")]
        public long? LastPostId { get; set; }

        [Column("postsInfo")]
        public required string PostsInfo { get; set; }

        [Column("postsCount")]
        public int PostsCount {get; set;}
    }
}
