﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace UDV_TestApplication.Migrations
{
    public partial class PostsCounteradded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "postsCount",
                table: "profileInfo",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "postsCount",
                table: "profileInfo");
        }
    }
}
