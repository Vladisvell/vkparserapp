﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace UDV_TestApplication.Migrations
{
    public partial class StructureChange2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "postData");

            migrationBuilder.DropTable(
                name: "profilePosts");

            migrationBuilder.CreateTable(
                name: "profileInfo",
                columns: table => new
                {
                    profileID = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    domain = table.Column<string>(type: "text", nullable: false),
                    lastPostID = table.Column<long>(type: "bigint", nullable: true),
                    postsInfo = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_profileInfo", x => x.profileID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "profileInfo");

            migrationBuilder.CreateTable(
                name: "profilePosts",
                columns: table => new
                {
                    profileID = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_profilePosts", x => x.profileID);
                });

            migrationBuilder.CreateTable(
                name: "postData",
                columns: table => new
                {
                    dataId = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    appearances = table.Column<string>(type: "text", nullable: false),
                    ProfilePostsId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_postData", x => x.dataId);
                    table.ForeignKey(
                        name: "FK_postData_profilePosts_ProfilePostsId",
                        column: x => x.ProfilePostsId,
                        principalTable: "profilePosts",
                        principalColumn: "profileID");
                });

            migrationBuilder.CreateIndex(
                name: "IX_postData_ProfilePostsId",
                table: "postData",
                column: "ProfilePostsId");
        }
    }
}
