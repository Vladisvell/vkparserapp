﻿namespace UDV_TestApplication
{
    public class VkPost(long id, string text, long? ownerId)
    {
        public long? Id { get;} = id;
        public string Text { get;} = text;
        public long? OwnerId { get;} = ownerId;
    }
}
