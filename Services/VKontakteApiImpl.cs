﻿using VkNet;
using VkNet.Enums.Filters;
using VkNet.Model;

namespace UDV_TestApplication.Services
{
    public class VkontakteApi(IConfiguration configuration, ILogger<VkontakteApi> logger) : IVkontakteApi
    {
        private readonly VkApi _api = new();
        private bool _authorized = false;

        private void Authorize()
        {
            logger.LogDebug("Authenticating VKApi...");
            if (configuration.GetConnectionString("AccessMode") == "credentials")
            {
                _api.Authorize(new ApiAuthParams
                {
                    ApplicationId = ulong.Parse(configuration.GetConnectionString("VkApplicationId") ?? string.Empty),
                    Login = configuration.GetConnectionString("VkLogin"),
                    Password = configuration.GetConnectionString("VkPassword"),
                    Settings = Settings.All
                });
            }
            else
            {
                _api.Authorize(new ApiAuthParams
                {
                    AccessToken = configuration.GetConnectionString("VkAccessToken"),
                });
            }

            logger.LogInformation("Authentication successful.");
            logger.LogDebug("Successfully authenticated via login and password, retrieved access token: {_api.Token}", _api.Token);
            _authorized = true;
        }

        public async Task<WallGetObject> GetPosts(string domain, ulong count)
        {
            if(!_authorized)
                Authorize();
            var wallGetParams = new WallGetParams()
            {
                Domain = domain,
                Count = count
            };
            return await _api.Wall.GetAsync(wallGetParams);
        }
    }
}
