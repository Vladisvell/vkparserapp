﻿using UDV_TestApplication.Entities;

namespace UDV_TestApplication.Services
{
    public interface IDbContexter
    {
        public Task AddProfileInfo(ProfileInfo info);
        public Task<ProfileInfo?> GetProfileInfo(string domain);
    }
}
