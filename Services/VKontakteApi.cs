﻿using VkNet.Model;

namespace UDV_TestApplication.Services
{
    public interface IVkontakteApi
    {
        public Task<WallGetObject> GetPosts(string domain, ulong count);
    }
}
