﻿using Microsoft.EntityFrameworkCore;
using UDV_TestApplication.Context;
using UDV_TestApplication.Entities;

namespace UDV_TestApplication.Services
{
    public class DbContexter(IDbContextFactory<DataContext> myDbContextFactory) : IDbContexter
    {
        public async Task AddProfileInfo(ProfileInfo info)
        {
            var _db = await myDbContextFactory.CreateDbContextAsync();
            await _db.AddAsync(info);
            await _db.SaveChangesAsync();
        }

        public async Task<ProfileInfo?> GetProfileInfo(string domain)
        {
            var _db = await myDbContextFactory.CreateDbContextAsync();
            return await _db.ProfileInfos.FirstOrDefaultAsync(x => x.Domain == domain);
        }
    }
}
