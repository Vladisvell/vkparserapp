using Microsoft.Extensions.DependencyInjection.Extensions;
using NReco.Logging.File;
using UDV_TestApplication.Context;
using UDV_TestApplication.Services;
using VkNet;
using VkNet.Enums.Filters;
using VkNet.Model;

namespace UDV_TestApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddDbContextFactory<DataContext>();
            builder.Services.AddSingleton<IVkontakteApi, VkontakteApi>();
            builder.Services.AddSingleton<IDbContexter, DbContexter>();
            builder.Services.AddLogging(loggingBuilder =>
            {
                var loggingSection = builder.Configuration.GetSection("Logging");
                loggingBuilder.AddFile(loggingSection);
            });
            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();
            
            
            var app = builder.Build();
            // Configure the HTTP request pipeline.
            app.UseSwagger();
            app.UseSwaggerUI();
            
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}
