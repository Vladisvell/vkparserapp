﻿using Microsoft.EntityFrameworkCore;
using UDV_TestApplication.Entities;

namespace UDV_TestApplication.Context
{
    public class DataContext(IConfiguration configuration) : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseNpgsql(configuration.GetConnectionString("DbConnectionString") ?? string.Empty);
        }

        public DbSet<ProfileInfo> ProfileInfos { get; set; } = null!;
    }
}
