using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using UDV_TestApplication.Context;
using UDV_TestApplication.Entities;
using UDV_TestApplication.Services;
using VkNet.Model;

namespace UDV_TestApplication.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class VkController(IDbContexter contexter, ILogger<VkController> logger, IVkontakteApi api)
        : ControllerBase
    {
        [HttpGet(Name = "Get character occurrences from last five posts from someone's VK wall.")]
        public async Task<JsonResult> GetLastFivePostsInfo(string domain)
        {
            return await GetLastCountPostsInfo(domain, 5);
        }

        [HttpGet(Name = "Get character occurrences from last certain count of posts from someone's VK wall.")]
        public async Task<JsonResult> GetLastCountPostsInfo(string domain, ulong count)
        {
            logger.LogInformation("Received query for retrieving info from {domain}, posts count: {count}", domain, count);
            var postsInfo = await contexter.GetProfileInfo(domain);
            if (postsInfo == null || postsInfo.PostsCount != (int)count)
            {
                logger.LogInformation("Couldn't get information from database, retrieving from Vkontakte Api...");
                var wallPostsInfo = await api.GetPosts(domain, count);
                logger.LogInformation("Retrieved information from {domain}, actual posts count: {wallPostsInfo.WallPosts.Count}",
                    domain, wallPostsInfo.WallPosts.Count);
                var lastPost = wallPostsInfo.WallPosts.FirstOrDefault();
                if (lastPost == null)
                {
                    logger.LogInformation("Retrieved no posts from {domain}", domain);
                    return new JsonResult($"Retrieved no posts from vk.com/{domain}, perhaps you should try again later?");
                }
                var lastPostId = lastPost.Id;
                var charOccurrences = GetCharOccurrences(wallPostsInfo);
                var info = JsonConvert.SerializeObject(charOccurrences);
                postsInfo = new ProfileInfo
                {
                    Domain = domain,
                    PostsInfo = info,
                    LastPostId = lastPostId,
                    PostsCount = wallPostsInfo.WallPosts.Count
                };
                logger.LogInformation("Adding info about {domain} and last {count} posts for later use...", domain, count);
                await contexter.AddProfileInfo(postsInfo);
            }
            else
            {
                logger.LogInformation("Retrieved information about domain {domain} from database, posts count: {count}", domain, count);
            }

            return new JsonResult(JsonConvert.DeserializeObject<Dictionary<char, int>>(postsInfo.PostsInfo));
        }

        [HttpGet(Name = "Get last of certain count of posts from VK wall.")]
        public async Task<IEnumerable<VkPost>> GetPosts(string domain, ulong count)
        {
            var wallPostsInfo = await api.GetPosts(domain, count);
            logger.LogInformation("Retrieved {count} posts from {domain} domain.", domain, count);
            return wallPostsInfo.WallPosts.Select(x => new VkPost((long)x.Id!, x.Text, x.OwnerId)).ToArray();
        }

        private static Dictionary<char, int> GetCharOccurrences(WallGetObject wall)
        {
            var chars = new Dictionary<char, int>();
            var unifiedStr = string.Join("", wall.WallPosts.Select(x => x.Text));
            foreach (var character in unifiedStr)
            {
                if (!char.IsLetter(character))
                    continue;
                var toProcess = char.ToLowerInvariant(character);
                if (!chars.TryAdd(toProcess, 1))
                    chars[char.ToLowerInvariant(character)] += 1;
            }

            return chars.OrderBy(x => x.Key).ToDictionary();
        }
    }
}