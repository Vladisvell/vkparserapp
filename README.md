## Приложение для сбора данных со стен пользователей и сообществ сервиса VKontakte.

Приложение, позволяющее собирать и хранить статистику о вхождениях одинаковых букв, с сортировкой и использованием базы данных PostgreSQL.

Использует SwaggerUI для взаимодействия.

Необходимые зависимости:

- PostgreSQL версии 15
- Dot.net 8.0
- Приложение Вконтакте https://vk.com/apps?act=manage, инструкции по созданию см. в документации.

Как запустить:

- Клонируйте репозиторий: git clone https://gitlab.com/Vladisvell/vkparserapp.git
- Заполните файл appsettings.json данными: строка подключения к базе данных PostgreSQL, ID вашего приложения VK, e-mail/номер телефона, пароль для доступа к API Вконтакте.
- Запустите проект в Visual Studio 2022. Если пакеты NuGet не были загружены автоматически, нажмите пкм по Solution -> Restore NuGet packages...
- В Package Manager Console (консоли менеджера пакетов) используйте команду `dotnet ef database update` , это создаст базу данных и нужные таблицы.

После успешных действий можете приступать к работе с приложением.

Формат работы с API приложения: SwaggerUI.

Выдача результатов работы приложения осуществляется в формате Json
